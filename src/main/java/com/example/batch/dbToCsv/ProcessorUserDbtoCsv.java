package com.example.batch.dbToCsv;

import com.example.batch.dto.UserPost;
import com.example.batch.model.Post;
import com.example.batch.model.User;
import com.example.batch.repository.PostRepository;
import lombok.AllArgsConstructor;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Configuration
@Qualifier("toCSV")
@AllArgsConstructor
public class ProcessorUserDbtoCsv {

    @Autowired
    private PostRepository postRepository;

@Bean("toCSV1")
public ItemProcessor<User, UserPost> filterDuplicatesProcessor() {
    return user -> {
            UserPost userPost = new UserPost();
            userPost.setDepartment(user.getDepartment());
            userPost.setTitle("from prcessor 1");
            userPost.setName(user.getName());
            userPost.setId(user.getId());
            userPost.setNumber(user.getNumber());
           return userPost;
    };
}

    @Bean("toCSV2")
    public ItemProcessor<UserPost, UserPost> getPostForUser() {
        return userPost -> {
            Optional<Post> post = postRepository.findFirstOptionalByUserId(userPost.getId());
            String title = post.map(post1 -> post1.getTitle()).orElse(null);

            userPost.setTitle(title);

            return userPost;
        };
    }
}