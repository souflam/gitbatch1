package com.example.batch.listener;

import com.example.batch.model.User;
import org.springframework.batch.core.ItemReadListener;

public class ReaderEventListenerDbToCsv implements ItemReadListener<User> {
    @Override
    public void beforeRead() {

    }

    @Override
    public void afterRead(User user) {

    }

    @Override
    public void onReadError(Exception e) {

    }
}
