package com.example.batch.listener;

import com.example.batch.dto.UserPost;
import com.example.batch.model.User;
import org.springframework.batch.core.ItemProcessListener;

public class ProcessorEventListnerDbToCsv implements ItemProcessListener<User, UserPost> {
    @Override
    public void beforeProcess(User user) {

    }

    @Override
    public void afterProcess(User user, UserPost userPost) {

    }

    @Override
    public void onProcessError(User user, Exception e) {

    }
}
