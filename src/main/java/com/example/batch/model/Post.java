package com.example.batch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Post implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Integer id;
    private String title;
    private String body;
    private Integer userId;


}
