package com.example.batch.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ChunkListener;
import org.springframework.batch.core.scope.context.ChunkContext;

public class ChunkListenerDbToCsv implements ChunkListener {
    Logger logger = LoggerFactory.getLogger(ChunkListenerDbToCsv.class);
    @Override
    public void beforeChunk(ChunkContext chunkContext) {
        logger.info("before chunk");
    }

    @Override
    public void afterChunk(ChunkContext chunkContext) {
        logger.info("After chunk");
    }

    @Override
    public void afterChunkError(ChunkContext chunkContext) {
        logger.info("After chunk Error");
    }
}
