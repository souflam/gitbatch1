package com.example.batch.batchUser;

import com.example.batch.model.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("toDB")
public class ProcessorUser implements ItemProcessor<User, User> {


    @Override
    public User process(User user) throws Exception {
         user.setName(user.getName().toUpperCase());
         return user;
    }
}
