package com.example.batch.dbToCsv;

import com.example.batch.dto.UserPost;
import org.springframework.batch.item.database.ItemPreparedStatementSetter;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UserPreparedStatmentSetter implements ItemPreparedStatementSetter<UserPost> {
    @Override
    public void setValues(UserPost userPost, PreparedStatement preparedStatement) throws SQLException {
        //if(userPost.getName().equalsIgnoreCase("agadir")) throw  new SQLException();
        preparedStatement.setDouble(1, userPost.getDepartment());
        preparedStatement.setDouble(2, userPost.getNumber());
        preparedStatement.setString(3, userPost.getName());
    }
}
