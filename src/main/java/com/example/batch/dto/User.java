package com.example.batch.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@AllArgsConstructor @NoArgsConstructor
@Data
public class User {
    private Integer id;
    @NotEmpty
    private String name;
    private Double number;
    private Double department;
    private String title;
}
