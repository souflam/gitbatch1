package com.example.batch.listener;

import com.example.batch.dto.UserPost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemWriteListener;

import java.util.List;

public class WriterEventListnerDbToCsv implements ItemWriteListener<UserPost> {

    Logger logger = LoggerFactory.getLogger(WriterEventListnerDbToCsv.class);
    @Override
    public void beforeWrite(List<? extends UserPost> list) {

    }

    @Override
    public void afterWrite(List<? extends UserPost> list) {

    }

    @Override
    public void onWriteError(Exception e, List<? extends UserPost> list) {
        logger.error(e.getMessage());
    }
}
