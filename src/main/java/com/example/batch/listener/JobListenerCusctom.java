package com.example.batch.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

public class JobListenerCusctom implements JobExecutionListener {
    private Logger logger = LoggerFactory.getLogger(JobListenerCusctom.class);

    @Override
    public void beforeJob(JobExecution jobExecution) {
        if( jobExecution.getStatus() == BatchStatus.COMPLETED ){
            logger.error(jobExecution.getExitStatus().toString());
        }
        else if(jobExecution.getStatus() == BatchStatus.FAILED){
            logger.error("OOK"+jobExecution.getExitStatus().toString());
        }
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        if( jobExecution.getStatus() == BatchStatus.COMPLETED ){
            logger.error(jobExecution.getExitStatus().toString());
        }
        else if(jobExecution.getStatus() == BatchStatus.FAILED){
        logger.error("OOK"+jobExecution.getExitStatus().toString());
        }
    }
}
