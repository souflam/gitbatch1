package com.example.batch.dbToCsv;

import com.example.batch.dto.UserPost;
import com.example.batch.listener.*;
import com.example.batch.model.User;
import com.example.batch.repository.UserRepository;
import org.springframework.batch.core.ChunkListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.parsing.ReaderEventListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Sort;

import javax.sql.DataSource;
import java.util.*;

@Configuration
@EnableBatchProcessing
@Qualifier("toCSV")
public class ConfigDbToCsv {
    @Autowired private JobBuilderFactory jobBuilderFactory;
    @Autowired private StepBuilderFactory stepBuilderFactory;
    @Autowired private DataSource dataSource;
    @Autowired private UserRepository userRepository;

    @Qualifier("toCSV")
    @Autowired private RepositoryItemReader<User> itemReader;

    @Autowired
    @Qualifier("CompositeProcessor")
    private ItemProcessor<User, UserPost> compositeProcessor;



    @Bean
    public Job jobToScv() {
        Step step = stepBuilderFactory.get("user_step_1_to_csv")
                .<User, UserPost>chunk(2)
                .reader(itemReader)
                .processor(compositeProcessor)
                .writer(compositeItemWriter())
                .listener(new StepExecutionListenerDbToCsv())
                .listener(new ChunkListenerDbToCsv())
                .listener(new ReaderEventListenerDbToCsv())
                .listener(new ProcessorEventListnerDbToCsv())
                .listener(new WriterEventListnerDbToCsv())
                .listener(new SkipListenerDbToCsv())
                .build();
        Job job = jobBuilderFactory.get("user_job_1_to_csv")
                .start(step)
                .listener(new JobListenerCusctom())
                .build();
        return job;
    }

    @Bean("CompositeProcessor")
    @StepScope
    public CompositeItemProcessor<User, UserPost> compositeProcessor(
            @Qualifier("toCSV1") ItemProcessor<User, UserPost> processor1,
           @Qualifier("toCSV2") ItemProcessor<UserPost, UserPost> processorGetPost
    ) {
        CompositeItemProcessor<User, UserPost> compositeItemProcessor = new CompositeItemProcessor<>();
        compositeItemProcessor.setDelegates(Arrays.asList(processor1, processorGetPost));
        return compositeItemProcessor;
    }

    @Bean
    @StepScope
    @Qualifier("toCSV")
    public CompositeItemWriter<UserPost> compositeItemWriter() {
        CompositeItemWriter<UserPost> userPostCompositeItemWriter = new CompositeItemWriter<>();

        userPostCompositeItemWriter.setDelegates(Arrays.asList(writeToFile(),writerFcIntroduction()));
        return userPostCompositeItemWriter;
    }

    public FlatFileItemWriter<UserPost> writeToFile() {
        FlatFileItemWriter<UserPost> delimitedWriter = new FlatFileItemWriter<>();
        delimitedWriter.setResource(new FileSystemResource("src/main/resources/dbtofile.csv"));
        delimitedWriter.setAppendAllowed(false);
        delimitedWriter.setLineAggregator(delimitedLineAggregator());
        return delimitedWriter;
    }

    private LineAggregator<UserPost> delimitedLineAggregator() {
        DelimitedLineAggregator<UserPost> lineAggregator = new DelimitedLineAggregator<>();
        lineAggregator.setDelimiter(",");
        lineAggregator.setFieldExtractor(fieldExtractor());
        return lineAggregator;
    }

    private BeanWrapperFieldExtractor<UserPost> fieldExtractor() {
        BeanWrapperFieldExtractor<UserPost> fieldExtractor = new BeanWrapperFieldExtractor<>();
        fieldExtractor.setNames(new String[]{"id", "name", "number", "department", "title", "idPost"});
        return fieldExtractor;
    }

    private JdbcBatchItemWriter<UserPost> writerFcIntroduction() {
        JdbcBatchItemWriter<UserPost> writer = new JdbcBatchItemWriter<>();
        writer.setDataSource(this.dataSource);
        writer.setSql("INSERT INTO user(department,number, name) VALUES(?,?,?)");
        writer.setItemPreparedStatementSetter(new UserPreparedStatmentSetter());
        writer.afterPropertiesSet();
        return writer;

    }
    @Bean
    @Qualifier("toCSV")
    public RepositoryItemReader<User> reader() {
        RepositoryItemReader<User> reader = new RepositoryItemReader<>();
        reader.setRepository(userRepository);
        reader.setMethodName("findAll");

        Map<String, Sort.Direction> sort = new HashMap<String, Sort.Direction>();
        sort.put("id", Sort.Direction.ASC);
        reader.setSort(sort);

        return reader;
    }

}
