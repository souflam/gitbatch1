package com.example.batch.listener;

import com.example.batch.dto.UserPost;
import com.example.batch.model.User;
import org.springframework.batch.core.SkipListener;

public class SkipListenerDbToCsv implements SkipListener<User, UserPost> {
    @Override
    public void onSkipInRead(Throwable throwable) {

    }

    @Override
    public void onSkipInWrite(UserPost userPost, Throwable throwable) {

    }

    @Override
    public void onSkipInProcess(User user, Throwable throwable) {

    }
}
