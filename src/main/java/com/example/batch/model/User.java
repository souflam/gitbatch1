package com.example.batch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class User {
    @Id
    private Integer id;
    @NotEmpty
    private String name;
    private Double number;
    private Double department;
}
