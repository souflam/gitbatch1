package com.example.batch.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserPost {
    private Integer id;
    private String name;
    private Double number;
    private Double department;
    private String title;
    private Double idPost;
}
